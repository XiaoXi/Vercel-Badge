# Vercel Badge

⭐ 为你的项目添加一个 Vercel 徽章 ⭐

[![vercel](https://vercelbadge.soraharu.com/?app=badge-for-vercel)](https://interactivehtmlbom.soraharu.com/)

## 🍭 使用说明

本项目的 API 接口为 `https://vercelbadge.soraharu.com/?app={VERCEL_APP_NAME}`。

要调用本项目的 API，首先，你需要获取你的 `{VERCEL_APP_NAME}`，对于 `https://badge-for-vercel.vercel.app` 来说，它的 `{VERCEL_APP_NAME}` 为 `badge-for-vercel`。

然后，在你的 `README.md` 中引用它：

### HTML

```html
<img src="https://vercelbadge.soraharu.com/?app={VERCEL_APP_NAME}" />
```

### Markdown

```markdown
![Vercel](https://vercelbadge.soraharu.com/?app={VERCEL_APP_NAME})
```

当然，你也可以为徽章追加特定的链接：

```markdown
[![Vercel](https://vercelbadge.soraharu.com/?app={VERCEL_APP_NAME})](https://gitlab.soraharu.com/XiaoXi/Vercel-Badge)
```

### 其他语法

- `?style=` - 徽章样式。支持的样式：`flat` (默认)、`flat-square`、`plastic`、`for-the-badge`
- `?root=` - 如果要检查位于其他路径的部署，请使用此参数。
  - 例如：要检查位于 `https://badge-for-vercel.vercel.app/projects/vercel-badge` 的部署，你需要将 root 指定为 `projects/vercel-badge`
- `?logo=` - 如果要禁用 徽章上的 vercel 标志，请使用此参数。允许的值：`true` (默认)、`false`

如果需要组合参数，请使用 **`&`** 作为连接字符。例如：

```
https://vercelbadge.soraharu.com/?app=badge-for-vercel&style=for-the-badge&logo=false
```

## ✨ 全部样式

| `flat` (默认)                                        | `flat-square`                                               | `plastic`                                               | `for-the-badge`                                              |
| ---------------------------------------------------- | ----------------------------------------------------------- | ------------------------------------------------------- | ------------------------------------------------------------ |
| ![Vercel](./Images/README/vercel-deployed-flat.svg)  | ![Vercel](./Images/README/vercel-deployed-flat-square.svg)  | ![Vercel](./Images/README/vercel-deployed-plastic.svg)  | ![Vercel](./Images/README/vercel-deployed-for-the-badge.svg) |
| ![Vercel](./Images/README/vercel-not-found-flat.svg) | ![Vercel](./Images/README/vercel-not-found-flat-square.svg) | ![Vercel](./Images/README/vercel-not-found-plastic.svg) | ![Vercel](./Images/README/vercel-not-found-for-the-badge.svg) |
| ![Vercel](./Images/README/vercel-failed-flat.svg)    | ![Vercel](./Images/README/vercel-failed-flat-square.svg)    | ![Vercel](./Images/README/vercel-failed-plastic.svg)    | ![Vercel](./Images/README/vercel-failed-for-the-badge.svg)   |

## 📜 开源许可

基于 [MIT License](https://choosealicense.com/licenses/mit/) 许可进行开源。
